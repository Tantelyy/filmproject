package producteur.film.filmproject.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import producteur.film.filmproject.model.Action;
import producteur.film.filmproject.model.Scene;
import producteur.film.filmproject.model.Tabletemporaire;
import producteur.film.filmproject.utiles.Utiles;
import producteur.film.filmproject.model.V_scene;

import java.io.Serializable;
import java.sql.Time;
import java.util.List;
import java.util.Vector;

//Hibernate 3.0
public class HibernateDao {

    private SessionFactory sessionFactory;

    public <T> List<T> findAllScene(Class<T> tClass, int idFilm){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            List<T> results = session.createCriteria(tClass)
                    .add(Restrictions.sqlRestriction("idfilm="+idFilm))
                    .list();

            return results;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }

    }

    public <T> T create(T entity) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.saveOrUpdate(entity);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            if (session != null) session.close();
        }
        return entity;
    }

    public <T> T findById(Class<T> clazz,Serializable id){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            T entity = (T) session.get(clazz, id);

            return entity;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    public <T> List<T> findAll(Class<T> tClass){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            List<T> results = session.createCriteria(tClass).list();

            return results;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }

    }

    public <T> List<T> findWhere(T entity){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Example example = Example.create(entity).ignoreCase();
            List<T> results = session.createCriteria(entity.getClass()).add(example).list();

            return results;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }

    }

    public <T> List<T> findSceneWhere(Class<T> clazz){
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(clazz)
                .add(Restrictions.sqlRestriction("idstatut<4"))
                .list();
        session.close();
        return results;
    }

    public <T> List<T> paginateWhere (T entity, int offset, int size){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Example example = Example.create(entity).ignoreCase();
            List<T> results = session.createCriteria(entity.getClass())
                    .add(example)
                    .setFirstResult(offset)
                    .setMaxResults(size).list();

            return results;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }

    }

    public <T> List<T> paginate(Class<T> clazz, int offset, int size){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            List<T> results = session.createCriteria(clazz)
                    .setFirstResult(offset)
                    .setMaxResults(size).list();

            return results;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    public <T> List<T> paginate(Class<T> clazz, int offset, int size, String orderBy, boolean orderAsc){
        Session session = null;
        try {
            session = sessionFactory.openSession();
            Order order = (orderAsc) ? Order.asc(orderBy) : Order.desc(orderBy);
            List<T> results = session.createCriteria(clazz)
                    .addOrder(order)
                    .setFirstResult(offset)
                    .setMaxResults(size).list();

            return results;
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }

    }

    public void deleteById(Class tClass, Serializable id){
        delete(findById(tClass, id));
    }

    public void delete(Object entity){
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.delete(entity);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    public <T> T update(T entity){
        Session session = null;
        Transaction transaction = null;
        try {
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            session.saveOrUpdate(entity);
            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            if (session != null) session.close();
        }
        return entity;
    }



    public Scene getFirst(Vector<Integer> id,Integer idFilm,String date, String[] ids) throws Exception{
        Session session = null;
        String sql = null;
        Scene result = null;
        try{
            session = sessionFactory.openSession();
            sql = Utiles.sqlForGetFirst(id,idFilm,date, ids);
            System.out.println("ito le sql "+sql);
            result =(Scene) session.createSQLQuery(sql).addEntity(Scene.class).uniqueResult();
        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
        return result;

    }

    public Scene getNext(Vector<Integer> id, Scene avant, Time debut,Integer idFilm,String date, String[] idScenes) throws Exception{
        Session session = null;
        String sql = null;
        Scene result = null;
        try{
            session = sessionFactory.openSession();
            sql = Utiles.sqlForGetNext(id,avant,debut,idFilm,date, idScenes);
            result =(Scene) session.createSQLQuery(sql).addEntity(Scene.class).uniqueResult();
            System.out.println(result+" tay");

        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
        return result;

    }

    public Scene getScene(int idScene) throws Exception{
        Session session = null;
        String sql = null;
        Scene result = null;
        try{
            session = sessionFactory.openSession();
            sql = "SELECT * FROM v_suggestion WHERE id="+idScene;
            result =(Scene) session.createSQLQuery(sql).addEntity(Scene.class).uniqueResult();

        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
        return result;

    }

    public void deleteTemporary(int idScene) throws Exception {
        Session session = null;
        String sql = null;
        Transaction transaction = null;
        try{
            session = sessionFactory.openSession();
            transaction = session.beginTransaction();
            sql = "DELETE FROM tabletemporaire WHERE idscene="+idScene;
            session.createSQLQuery(sql).addEntity(Scene.class).executeUpdate();
            transaction.commit();
        }
        catch (Exception e){
            transaction.rollback();
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }

    }


    public List<Scene> getSceneBYSuggestion(Integer idFilm) throws Exception{

        Session session = null;
        String sql = null;
        List<Scene> result = null;
        try{
            session = sessionFactory.openSession();
            sql = "SELECT * FROM v_suggestion2 WHERE idfilm="+idFilm+" ORDER BY idplateau ASC";
            result =(List<Scene> )session.createSQLQuery(sql).addEntity(Scene.class).list();

        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
        return result;
    }


    public void setProposalPlanning(Vector<Scene> scene,Session session) throws Exception{
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            for(Scene s : scene){
                Tabletemporaire temp = new Tabletemporaire();
                temp.setIdScene(s.getId());
                temp.setPlanning(s.getDatePlanning());
                this.create(temp);
            }
            transaction.commit();


        }
        catch (Exception e){
            transaction.rollback();
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
    }


    public Double getDuree(Integer idScene) throws Exception{

        Session session = null;
        String sql = null;
        List<Action> result = null;
        double duree = 0.0;
        try{
            session = sessionFactory.openSession();
            sql = "SELECT * FROM Action WHERE idscene="+idScene;
            result =(List<Action> )session.createSQLQuery(sql).addEntity(Action.class).list();

            for(Action a : result){
                duree+=a.getDuree();
            }

        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
        return duree;
    }



    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}