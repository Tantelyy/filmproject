package producteur.film.filmproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.Indisponibilite;
import producteur.film.filmproject.model.Plateau;

@Controller
public class PlateauController {

    @Autowired
    private HibernateDao hibernateDao;

    public HibernateDao getHibernateDao() {
        return hibernateDao;
    }

    public void setHibernateDao(HibernateDao hibernateDao) {
        this.hibernateDao = hibernateDao;
    }

    @GetMapping("/indisponibilite")
    public ModelAndView indisponibilite() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("plateaux", hibernateDao.findAll(Plateau.class));
        modelAndView.setViewName("PlateauIndisponible");

        return modelAndView;
    }

    @PostMapping("/addIndisponibilite")
    public ModelAndView addIndisponibilite(@ModelAttribute Indisponibilite indisponibilite) {
        hibernateDao.create(indisponibilite);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("plateaux", hibernateDao.findAll(Plateau.class));
        modelAndView.setViewName("PlateauIndisponible");

        return modelAndView;
    }
}
