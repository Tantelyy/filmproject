package producteur.film.filmproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.*;
import producteur.film.filmproject.service.SceneService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Controller
public class PlanningController {
    @Autowired
    private HibernateDao hibernateDao;

    @Autowired
    private SceneService sceneService;

    @Autowired
    private HomeController homeController;

    public HomeController getHomeController() {
        return homeController;
    }

    public void setHomeController(HomeController homeController) {
        this.homeController = homeController;
    }

    public SceneService getSceneService() {
        return sceneService;
    }

    public void setSceneService(SceneService sceneService) {
        this.sceneService = sceneService;
    }

    public HibernateDao getHibernateDao() {
        return hibernateDao;
    }

    public void setHibernateDao(HibernateDao hibernateDao) {
        this.hibernateDao = hibernateDao;
    }

    public int getNbrPage(List<Film> films) {
        int taille = films.size();
        int nbr = 1;

        if(taille > 3) {
            nbr = taille/3;
            int r = taille%3;
            if(r > 0) {
                nbr+=1;
            }
        }
        return nbr;
    }

    @GetMapping("/redirectPlan")
    public ModelAndView redirectPlan(@RequestParam int id) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("idFilm", id);
        modelAndView.addObject("scenes", hibernateDao.findAllScene(Scene.class, id));

        modelAndView.setViewName("Planifier");

        return modelAndView;
    }

    @GetMapping("/redirectConfig")
    public ModelAndView redirectCongig(@RequestParam int id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("idFilm", id);
        modelAndView.setViewName("Configuration");

        return modelAndView;
    }


    @PostMapping("/config")
    public ModelAndView config(@ModelAttribute FilmPlanning filmPlanning, @RequestParam(name = "totalTournage") String totalTournage, @RequestParam(name = "debutTournage") String debutTournage) {
        totalTournage += ":00";
        debutTournage += ":00";

        Time heureTotalJournalier = Time.valueOf(totalTournage);
        Time heureDebutJournalier = Time.valueOf(debutTournage);

        filmPlanning.setHeureTotalJournalier(heureTotalJournalier);
        filmPlanning.setHeureDebutJournalier(heureDebutJournalier);

        hibernateDao.create(filmPlanning);
        List<Film> films = hibernateDao.findAll(Film.class);


        return this.redirectPlan(filmPlanning.getIdFilm());
    }

    @PostMapping("/planifier")
    public ModelAndView planifier(@RequestParam(name = "idFilm") String id, HttpSession session, @RequestParam(name = "dateDebut") String dateDebut, @RequestParam(name = "dateFin") String dateFin, @RequestParam(name = "idscene") String[] idscene) throws Exception {
        int idFilm = Integer.parseInt(id);

        //String[] ids = scene.split(",");

        List<PlanningPlateau> liste = sceneService.setPlanning(idFilm, dateDebut, dateFin, idscene);
        session.setAttribute("planning", liste);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("planning", liste);
        modelAndView.addObject("idFilm", idFilm);
        modelAndView.addObject("film", hibernateDao.findById(Film.class, idFilm));
        modelAndView.addObject("dateDebut", dateDebut);
        modelAndView.addObject("dateFin", dateFin);
        modelAndView.setViewName("Planning");

        return modelAndView;
    }

    @GetMapping("/valideTous")
    public ModelAndView valideTous(HttpSession session) throws Exception{
        List<PlanningPlateau> planning = (List<PlanningPlateau>) session.getAttribute("planning");
        for (PlanningPlateau pl : planning) {
            for(Scene scene: pl.getScenePLaning())
            sceneService.toValidSceneInPlanning(scene.getId());
        }

        return homeController.liste(0);
    }

    @PostMapping("/validateOne")
    public ModelAndView validateOne(HttpSession session, @RequestParam(name = "idScene") String idScene, @RequestParam(name = "idFilm") int idFilm, @RequestParam(name = "dateDebut") String dateDebut, @RequestParam(name = "dateFin") String dateFin) throws Exception{
        ModelAndView modelAndView = new ModelAndView();
        int scene = Integer.parseInt(idScene);
        Scene sce = sceneService.toValidSceneInPlanning(scene);

        Indisponibilite indisponibilite = new Indisponibilite();

        indisponibilite.setIdPlateau(sce.getPlateau().getId());
        indisponibilite.setJour(sce.getDatePlanning());
        indisponibilite.setObservation("Indisponible pour tournage");

        hibernateDao.create(indisponibilite);

        session.removeAttribute("planning");

        //String[] ids = new String[]{"1", "2"};

        List<PlanningPlateau> liste = sceneService.listPlanningToValid(idFilm);
        session.setAttribute("planning", liste);

        // List<PlanningPlateau> planning = (List<PlanningPlateau>) session.getAttribute("planning");
        modelAndView.addObject("planning", liste);
        modelAndView.addObject("idFilm", idFilm);
        modelAndView.addObject("dateDebut", dateDebut);
        modelAndView.addObject("dateFin", dateFin);
        modelAndView.addObject("film", hibernateDao.findById(Film.class, idFilm));
        modelAndView.setViewName("Planning");


        /*
        session.removeAttribute("planning");

        String[] ids = new String[]{"1", "2"};

        List<PlanningPlateau> liste = sceneService.setPlanning(idFilm, dateDebut, dateFin, ids);
        session.setAttribute("planning", liste);

        // List<PlanningPlateau> planning = (List<PlanningPlateau>) session.getAttribute("planning");
        modelAndView.addObject("planning", liste);
        modelAndView.addObject("idFilm", idFilm);
        modelAndView.addObject("dateDebut", dateDebut);
        modelAndView.addObject("dateFin", dateFin);
        modelAndView.addObject("film", hibernateDao.findById(Film.class, idFilm));
        modelAndView.setViewName("Planning");*/

        return modelAndView;
    }

    @GetMapping("/planningValide")
    public ModelAndView planningValide(@RequestParam int idFilm) throws Exception{
        ModelAndView modelAndView = new ModelAndView();
        List<Scene> scenes = hibernateDao.findAllScene(Scene.class, idFilm);

        modelAndView.addObject("planning", scenes);
        modelAndView.addObject("film", hibernateDao.findById(Film.class, idFilm));
        modelAndView.setViewName("PlanningValide");

        return modelAndView;
    }
}
