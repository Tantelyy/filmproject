package producteur.film.filmproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.List;

@Controller
public class SceneController {
    @Autowired
    private HibernateDao hibernateDao;

    public int getNbrPage(List<Action> actions) {
        int taille = actions.size();
        int nbr = 1;

        if(taille > 3) {
            nbr = taille/3;
            int r = taille%3;
            if(r > 0) {
                nbr+=1;
            }
        }
        return nbr;
    }


    @GetMapping("/redirectAddScene")
    public ModelAndView redirectAddScene(@RequestParam int id) {
        ModelAndView modelAndView = new ModelAndView();

        List<Plateau> plateaux = hibernateDao.findAll(Plateau.class);

        List<Scene> scenes = hibernateDao.findAllScene(Scene.class, id);

        modelAndView.addObject("scenes", scenes);
        modelAndView.addObject("idFilm", id);
        modelAndView.addObject("plateaux", plateaux);
        modelAndView.addObject("personnages", hibernateDao.findAll(Personnage.class));
        modelAndView.addObject("humeurs", hibernateDao.findAll(Humeur.class));

        modelAndView.setViewName("Scene");

        return modelAndView;
    }

    @PostMapping("/addScene")
    private ModelAndView addScene(@ModelAttribute Scene scene, @RequestParam(name = "min") String min, @RequestParam(name = "max") String max, @RequestParam CommonsMultipartFile file, HttpSession session) throws IOException {
        System.out.println(min);
        System.out.println(max);
        min += ":00";
        max += ":00";

        Time heureMin = Time.valueOf(min);
        Time heureMax = Time.valueOf(max);

        Plateau plateau = hibernateDao.findById(Plateau.class, scene.getIdPlateau());

        String path = session.getServletContext().getRealPath("/images");
        String fileName = Film.uploadPhoto(path, file);

        scene.setImage(fileName);
        scene.setHeureMax(heureMax);
        scene.setHeureMin(heureMin);
        scene.setPlateau(plateau);

        Statut statut = hibernateDao.findById(Statut.class, 1);
        scene.setStatut(statut);

        System.out.println(scene);

        Scene newScene = hibernateDao.create(scene);

        return this.redirectAddScene(scene.getIdFilm());
    }

    @GetMapping("/terminate")
    public ModelAndView terminate(@RequestParam int idScene, @RequestParam int idFilm) {
        Statut statut = new Statut();
        statut.setId(3);
        Scene scene = hibernateDao.findById(Scene.class, idScene);
        scene.setStatut(statut);
        hibernateDao.create(scene);

        return this.redirectAddScene(idFilm);
    }
}
