package producteur.film.filmproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.*;

import java.util.List;

@Controller
public class ActionController {
    @Autowired
    private HibernateDao hibernateDao;

    @Autowired
    private SceneController sceneController;

    @GetMapping("/redirectAddAction")
    private ModelAndView redirectAddAction(@RequestParam int id) {
        ModelAndView modelAndView = new ModelAndView();

        List<Personnage> personnages = hibernateDao.findAll(Personnage.class);

        List<Humeur> humeurs = hibernateDao.findAll(Humeur.class);

        modelAndView.addObject("idScene", id);
        modelAndView.addObject("personnages", personnages);
        modelAndView.addObject("humeurs", humeurs);

        modelAndView.setViewName("Action");

        return modelAndView;
    }

    @PostMapping("/addAction")
    private ModelAndView addAction(@ModelAttribute Action action) {
        ModelAndView modelAndView = new ModelAndView();

        if (action.getIdPersonnage() != null) {
            Personnage personnage = hibernateDao.findById(Personnage.class, action.getIdPersonnage());
            action.setPersonnage(personnage);
        }

        if (action.getIdHumeur() != null) {
            Humeur humeur = hibernateDao.findById(Humeur.class, action.getIdHumeur());
            action.setHumeur(humeur);
        }

        Action newAction = hibernateDao.create(action);

        Statut statut = hibernateDao.findById(Statut.class, 2);
        Scene scene = hibernateDao.findById(Scene.class, action.getIdScene());
        scene.setStatut(statut);
        hibernateDao.create(scene);

        return sceneController.redirectAddScene(action.getIdFilm());
    }
}
