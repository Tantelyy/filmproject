package producteur.film.filmproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.Action;
import producteur.film.filmproject.model.Film;
import producteur.film.filmproject.model.Scene;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    private HibernateDao hibernateDao;

    public int getNbrPage(List<Film> films) {
        int taille = films.size();
        int nbr = 1;

        if(taille > 3) {
            nbr = taille/3;
            int r = taille%3;
            if(r > 0) {
                nbr+=1;
            }
        }
        return nbr;
    }

    public List<Film> getPlanified() {
        List<Film> list = new ArrayList<Film>();
        List<Film> films = hibernateDao.findAll(Film.class);
        for(Film film : films) {
            boolean isPlanified = false;
            List<Scene> scenes = hibernateDao.findAllScene(Scene.class, film.getId());
            for(Scene scene : scenes) {
                if(scene.getStatut().getId() != 4 ){
                    isPlanified = false;
                    break;
                } else {
                    isPlanified = true;
                }
            }
            if(isPlanified) {
                list.add(film);
            }
        }
        return list;
    }

    public List<Film> getNotPlanified() {
        List<Film> list = new ArrayList<Film>();
        List<Film> films = hibernateDao.findAll(Film.class);
        for(Film film : films) {
            boolean isPlanified = false;
            List<Scene> scenes = hibernateDao.findAllScene(Scene.class, film.getId());
            for(Scene scene : scenes) {
                if(scene.getStatut().getId() == 4 ){
                    isPlanified = true;
                } else {
                    isPlanified = false;
                    break;
                }
            }
            if(!isPlanified) {
                list.add(film);
            }
        }
        return list;
    }

    @GetMapping("/")
    private ModelAndView home() {
      return this.liste(0);
    }

    @GetMapping("/liste")
    public ModelAndView liste(@RequestParam int offset) {
        ModelAndView modelAndView = new ModelAndView();

        List<Film> planified = this.getPlanified();
        List<Film> notPlanified = this.getNotPlanified();


        /*List<Film> paginate = hibernateDao.paginate(Film.class, offset, 3);
        int pages = this.getNbrPage(films);

        modelAndView.addObject("pages", pages);*/

        modelAndView.addObject("planified", planified);
        modelAndView.addObject("notPlanified", notPlanified);
        modelAndView.setViewName("Home");

        return modelAndView;
    }

    @PostMapping("/addFilm")
    private ModelAndView addFilm(@ModelAttribute Film film, @RequestParam CommonsMultipartFile file, HttpSession session) throws IOException {
        String path = session.getServletContext().getRealPath("/images");
        String fileName = Film.uploadPhoto(path, file);

        film.setPhoto(fileName);

        hibernateDao.create(film);

        return home();
    }
}
