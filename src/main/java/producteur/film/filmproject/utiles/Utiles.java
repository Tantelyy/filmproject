package producteur.film.filmproject.utiles;

import producteur.film.filmproject.model.Scene;
import producteur.film.filmproject.model.V_scene;

import java.sql.Time;
import java.util.Vector;

public class Utiles {


    public static String sqlForGetFirst(Vector<Integer> id,Integer idFilm,String date, String[] idScenes) {
        String sql="SELECT* FROM v_scene WHERE idplateau IN(SELECT idplateau from (SELECT idplateau,count(idplateau)nombre from scene WHERE idfilm= "+idFilm+" "+" AND idPlateau NOT IN (SELECT idplateau from indisponibilite where jour='"+date+"')  ";
        //if(id.size()>0) sql = sql+" AND  id!="+id.get(0)+" ";

        if (idScenes.length > 0) {
            sql += " AND (";

            for (String idScene: idScenes) {
                sql += "id="+idScene + " OR ";
            }
            sql = sql.substring(0, sql.length() - 4);
            sql += ") ";

        }

        for(int i=0;i<id.size();i++) {
            sql = sql+"AND id!="+id.get(i)+" ";
        }
        sql = sql+"GROUP BY idplateau ORDER BY nombre desc limit 1)plateau) ";

        if (idScenes.length > 0) {
            sql += " AND (";

            for (String idScene: idScenes) {
                sql += "id="+idScene + " OR ";
            }
            sql = sql.substring(0, sql.length() - 4);
            sql += ") ";

        }

        for(int i=0;i<id.size();i++) {
            sql = sql+"AND id!="+id.get(i)+" ";
        }
        sql = sql+"order by heureMin ASC limit 1";
        System.out.println(" firstquery "+sql);
        return sql;
    }



    public static String sqlForGetNext(Vector<Integer> id, Scene avant, Time debut,Integer idFilm,String date, String[] idScenes) throws Exception{
        String sql="SELECT v.*,sqrt((longitude-"+avant.getPlateau().getLongitude()+")^2+(latitude-"+avant.getPlateau().getLatitude()+")^2)distance "
                + "FROM V_plateau v WHERE (CAST('"+debut+"' AS time)+(CAST((duree||' minutes') AS interval)))<=heureMax AND  idFilm= "+idFilm+"  AND idPlateau NOT IN (SELECT idplateau from indisponibilite where jour='"+date+"')  ";

        if (idScenes.length > 0) {
            sql += " AND (";

            for (String idScene: idScenes) {
                sql += "id="+idScene + " OR ";
            }
            sql = sql.substring(0, sql.length() - 4);
            sql += ") ";

        }

        for(int i=0;i<id.size();i++) {
            sql = sql+"AND id!="+id.get(i)+" ";
        }
        sql = sql+"order by distance,heureMin ASC limit 1";
        System.out.println(" nextquery "+sql);
        return sql;
    }



}
