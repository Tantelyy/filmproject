package producteur.film.filmproject.service;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.*;

import java.sql.Time;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;

@Service
public class SceneService {
    @Autowired
    private HibernateDao hibernateDao;

    public HibernateDao getHibernateDao() {
        return hibernateDao;
    }

    public void setHibernateDao(HibernateDao hibernateDao) {
        this.hibernateDao = hibernateDao;
    }



    public Double getDuree(Scene scene) throws Exception{
        return hibernateDao.getDuree(scene.getId());
    }


    public  Vector<Scene> getPlanning(Integer idfilm, String date, String date2, String[] ids) throws Exception{
        Vector<Scene> valiny = new Vector<Scene>();
        //Integer idfilm = 1;
        Vector<Integer> id = new Vector<Integer>();
        Scene repere = null;
        Scene repere2 = null;
        //tocontinue
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateDebut = null;
        Date dateFin = sdf.parse(date2 + " 20:00:00");
        int x = 0;
        Time comparaison = null;
        boolean suite = true;

        try {

            dateDebut =  sdf.parse(date+" 08:00:00");

            Calendar calendar = Calendar.getInstance();

            while (true) {
                if (dateDebut.after(dateFin)) {
                    break;
                }
                //voalohany ao anaty andro iray
                suite = true;
                repere = hibernateDao.getFirst(id,idfilm,dateDebut.toString(), ids);

                if(repere==null && dateDebut.before(dateFin)) {
                    calendar.setTime(dateDebut);
                    calendar.add(Calendar.DATE,1);
                    dateDebut = new Date(calendar.getTimeInMillis());
                    continue;
                }else if(repere==null && dateDebut.after(dateFin)){
                    break;
                }
                comparaison = new Time(dateDebut.getTime());
                if((comparaison.getHours()<repere.getHeureMin().getHours()) || (comparaison.getHours() == repere.getHeureMin().getHours() && comparaison.getMinutes()<repere.getHeureMin().getMinutes())) {
                    //System.out.println("BEFOREE PRIM");
                    dateDebut.setHours(repere.getHeureMin().getHours());
                    dateDebut.setMinutes(repere.getHeureMin().getMinutes());
                }


                repere.setDatePlanning(dateDebut);
                /*System.out.println("PRIM "+repere.getId()+" "+repere.getTitre()+" "+repere.getHeureMin()+" "+repere.getHeureMax()+" idPLateau = "+repere.getPlateau().getNom()+" le : "+repere.getDatePlanning());
                System.out.println("---------------");*/
                x = repere.getId();
                id.add(x);
                valiny.add(repere);

                //calcul date fin an'ilay voalohany io,idiran'ilay manaraka

                Date fin = new Date(dateDebut.getTime());
                calendar.setTime(fin);
                calendar.add(Calendar.MINUTE, this.getDuree(repere).intValue());
                Time time = new Time(calendar.getTimeInMillis());

                //manampy date
                calendar.setTime(dateDebut);
                calendar.add(Calendar.MINUTE, this.getDuree(repere).intValue());
                dateDebut = new Date(calendar.getTimeInMillis());
                //System.out.println("dateCalendrier = "+dateDebut);

                while(suite) {
                    //System.out.println("NEXTT");
                    repere2 = hibernateDao.getNext(id, repere, time,idfilm,dateDebut.toString(), ids);

                    if(repere2 != null) {
                        //compare deux time ,ajuster heure debut
                        comparaison = new Time(dateDebut.getTime());
                        //System.out.println("COMPARAISON = "+comparaison+" VRAI = "+repere2.getHeureMin());
                        //if(comparaison.compareTo(repere2.getHeureMin())<0) {
                        if((comparaison.getHours()<repere2.getHeureMin().getHours()) || (comparaison.getHours() == repere2.getHeureMin().getHours() && comparaison.getMinutes()<repere2.getHeureMin().getMinutes())) {
                            //System.out.println("BEFOREE");
                            dateDebut.setHours(repere2.getHeureMin().getHours());
                            dateDebut.setMinutes(repere2.getHeureMin().getMinutes());
                        }


                        repere2.setDatePlanning(dateDebut);
                        calendar.setTime(dateDebut);
                        calendar.add(Calendar.MINUTE, this.getDuree(repere2).intValue());
                        dateDebut = new Date(calendar.getTimeInMillis());
                        time = new Time(calendar.getTimeInMillis());

                        if((dateDebut.getHours()>16) || (dateDebut.getHours()==16 && dateDebut.getMinutes()>0)) {
                            //System.out.println("FINNNNN");
                            calendar.add(Calendar.DATE,1);
                            dateDebut = new Date(calendar.getTimeInMillis());
                            dateDebut.setHours(repere2.getHeureMin().getHours());
                            dateDebut.setMinutes(repere2.getHeureMin().getMinutes());
                            //System.out.println("DATE DEBUT + = "+dateDebut);
                            suite = false;
                           // System.out.println("-----FALSE------");
                        }

                        /*System.out.println("dateCalendrier2 = "+dateDebut);
                        System.out.println(repere2.getId()+" "+repere2.getTitre()+" "+repere2.getHeureMin()+" "+repere2.getHeureMax()+" idPLateau = "+repere2.getPlateau().getNom()+" le : "+repere2.getDatePlanning());
                        System.out.println("---------------");*/
                        x = repere2.getId();
                        id.add(x);
                        valiny.add(repere2);

                    }else {
                        //System.out.println("FINNNNN NULL");
                        //System.out.println(id.size()+" SIZEEE");
                        calendar.add(Calendar.DATE,1);
                        dateDebut = new Date(calendar.getTimeInMillis());
                        dateDebut.setHours(8);
                        dateDebut.setMinutes(0);
                        suite = false;
                        //System.out.println("***********************");
                    }

                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }

        return valiny;
    }

    public  List<PlanningPlateau> getPlaningPerPlateau(Integer idfilm) throws Exception {
        //Integer idfilm = 1;
        List<Scene> data = hibernateDao.getSceneBYSuggestion(idfilm);
        PlanningPlateau planning = null;
        Vector<PlanningPlateau> ans = new Vector<PlanningPlateau>();
        int indice = -10;
        for(Scene scene : data){
            System.out.println(scene.getPlateau().getId()+"    "+indice);

            if(indice!=scene.getPlateau().getId()) {
                System.out.println("ato");
                if(indice>0) {
                    ans.add(planning);
                }
                indice = scene.getPlateau().getId();
                planning = new PlanningPlateau();
                planning.setPlateau(hibernateDao.findById(Plateau.class,indice));
            }
            planning.getScenePLaning().add(scene);

        }
        ans.add(planning);
        return ans;
    }


    public void savePlaning(Vector<Scene> planingResult ) throws Exception {
        Session session = null;
        try{
            session = this.hibernateDao.getSessionFactory().openSession();
            this.hibernateDao.setProposalPlanning(planingResult,session);
        }
        catch (Exception e){
            throw e;
        }
        finally {
            if(session!=null)session.close();
        }
    }


    //ito misave anle izy ao anaty tabletemporaire (le resultat anle planning vector scene
    public List<PlanningPlateau> setPlanning(Integer idFilm ,String dateDebut, String dateFin, String[] ids) throws Exception{
        Vector<Scene> planning  = this.getPlanning(idFilm,dateDebut, dateFin, ids);
        this.savePlaning(planning);
        return this.getPlaningPerPlateau(idFilm);
    }

    public List<PlanningPlateau> listPlanningToValid(Integer idFilm) throws Exception{
        return this.getPlaningPerPlateau(idFilm);
    }


    public Scene toValidSceneInPlanning(int idScene) throws Exception {
        Scene scene = hibernateDao.getScene(idScene);

        Statut planifiedStatut = hibernateDao.findById(Statut.class, 4);

        scene.setStatut(planifiedStatut);

        hibernateDao.deleteTemporary(scene.getId());

        Scene sceneUpdated = hibernateDao.create(scene);

        return sceneUpdated;

    }

}
