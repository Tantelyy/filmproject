package producteur.film.filmproject.runner;

import org.hibernate.Hibernate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import producteur.film.filmproject.dao.HibernateDao;
import producteur.film.filmproject.model.Film;
import producteur.film.filmproject.model.PlanningPlateau;
import producteur.film.filmproject.model.Scene;
import producteur.film.filmproject.model.V_scene;
import producteur.film.filmproject.service.SceneService;
import producteur.film.filmproject.utiles.Utiles;

import java.util.List;
import java.util.Vector;

public class Main {
    public static void main(String[] args)  {
        try{
            ApplicationContext context = new ClassPathXmlApplicationContext("spring-context.xml");
            HibernateDao hibernateDao = context.getBean(HibernateDao.class);
            String[] ids = new String[]{"1", "2", "3"};

            SceneService s = context.getBean(SceneService.class);
            Vector<Scene>  ss =s.getPlanning(1,"2023-03-17", "2023-03-18", ids);

            System.out.println(ss.size());
            for(Scene v : ss){
                System.out.println(v.toString());
            }



            Utiles.sqlForGetFirst(new Vector<>(), 1, "2023-03-17", ids);
            //s.savePlaning(ss);
            //Scene scene = hibernateDao.findById(Scene.class, 1);
            //System.out.println(scene.getActions());
            //System.out.println(hibernateDao.getSceneBYSuggestion(1));


           /* System.out.println(hibernateDao.findAll(Scene.class));
            for(Scene se : hibernateDao.findAll(Scene.class)){
                System.out.println(s.getDuree(se));
            }*/

            /*for(PlanningPlateau p : s.getPlaningPerPlateau(1)) {
                System.out.println(p.toString());
            }*/
            /* List<PlanningPlateau> pl = s.setPlanning(1, "2023-03-17", "");
            for(PlanningPlateau p : pl) {
                System.out.println("ito = " + p.toString());
            } */
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}
