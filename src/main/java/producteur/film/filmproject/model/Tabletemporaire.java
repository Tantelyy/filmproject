package producteur.film.filmproject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Tabletemporaire {
    @Id
    @Column(name = "idScene")
    private Integer idScene;
    @Column(name = "planning")
    private Date planning;


    public Integer getIdScene() {
        return idScene;
    }

    public void setIdScene(Integer idScene) {
        this.idScene = idScene;
    }

    public Date getPlanning() {
        return planning;
    }

    public void setPlanning(Date planning) {
        this.planning = planning;
    }
}
