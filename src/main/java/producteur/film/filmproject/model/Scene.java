package producteur.film.filmproject.model;

import org.springframework.beans.factory.annotation.Autowired;
import producteur.film.filmproject.dao.HibernateDao;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Entity
public class Scene {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "titre")
    private String titre;

    @Column(name = "image")
    private String image;

    @Transient
    private Integer idPlateau;

    @Column(name = "heureMin")
    private Time heureMin;

    @Column(name = "heureMax")
    private Time heureMax;

    @Column(name = "idFilm")
    private Integer idFilm;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = Plateau.class)
    @JoinColumn(name = "idPlateau")
    private Plateau plateau;

    @OneToOne
    @JoinColumn(name = "idStatut")
    private Statut statut;

    @Column(name = "dateplanning")
    private Date datePlanning;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public Integer getIdPlateau() {
        return idPlateau;
    }

    public void setIdPlateau(Integer idPlateau) {
        this.idPlateau = idPlateau;
    }

    public Integer getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(Integer idFilm) {
        this.idFilm = idFilm;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Time getHeureMin() {
        return heureMin;
    }

    public void setHeureMin(Time heureMin) {
        this.heureMin = heureMin;
    }

    public Time getHeureMax() {
        return heureMax;
    }

    public void setHeureMax(Time heureMax) {
        this.heureMax = heureMax;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public Statut getStatut() {
        return statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Date getDatePlanning() {
        return datePlanning;
    }

    public void setDatePlanning(Date datePlanning) {
        this.datePlanning = datePlanning;
    }

    @Override
    public String toString() {
        return "Scene{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", image='" + image + '\'' +
                ", idPlateau=" + idPlateau +
                ", heureMin=" + heureMin +
                ", heureMax=" + heureMax +
                ", idFilm=" + idFilm +
                ", plateau=" + plateau +
                ", statut=" + statut +
                ", datePlanning=" + datePlanning +
                '}';
    }

    /*public String toString() {
        return "Scene{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", image='" + image + '\'' +
                ", idPlateau=" + idPlateau +
                ", heureMin=" + heureMin +
                ", heureMax=" + heureMax +
                ", idFilm=" + idFilm +
                ", actions=" + actions +
                ", plateau=" + plateau +
                '}';
    }*/
}
