package producteur.film.filmproject.model;

import javax.persistence.*;

@Entity
public class Humeur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "valeur")
    private String valeur;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValeur() {
        return valeur;
    }

    public void setValeur(String valeur) {
        this.valeur = valeur;
    }

    @Override
    public String toString() {
        return "Humeur{" +
                "id=" + id +
                ", valeur='" + valeur + '\'' +
                '}';
    }
}
