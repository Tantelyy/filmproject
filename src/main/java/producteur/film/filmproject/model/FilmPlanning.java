package producteur.film.filmproject.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.sql.Time;

@Entity
public class FilmPlanning {

    @Id
    @Column(name = "idfilm")
    private Integer idFilm;

    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @Column(name = "datedebuttournage")
    private Date dateDebutTournage;

    @Column(name = "heuretotaljournalier")
    private Time heureTotalJournalier;

    @Column(name = "heuredebutjournalier")
    private Time heureDebutJournalier;


    @Column(name = "weekendimplique")
    private Boolean weekEndImplique;

    public Integer getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(Integer idFilm) {
        this.idFilm = idFilm;
    }

    public Date getDateDebutTournage() {
        return dateDebutTournage;
    }

    public void setDateDebutTournage(Date dateDebutTournage) {
        this.dateDebutTournage = dateDebutTournage;
    }

    public Time getHeureTotalJournalier() {
        return heureTotalJournalier;
    }

    public void setHeureTotalJournalier(Time heureTotalJournalier) {
        this.heureTotalJournalier = heureTotalJournalier;
    }

    public Time getHeureDebutJournalier() {
        return heureDebutJournalier;
    }

    public void setHeureDebutJournalier(Time heureDebutJournalier) {
        this.heureDebutJournalier = heureDebutJournalier;
    }

    public Boolean getWeekEndImplique() {
        return weekEndImplique;
    }

    public void setWeekEndImplique(Boolean weekEndImplique) {
        this.weekEndImplique = weekEndImplique;
    }
}
