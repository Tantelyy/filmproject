package producteur.film.filmproject.model;

import java.util.ArrayList;
import java.util.List;

public class PlanningPlateau {

    private Plateau plateau;
    private List<Scene> scenePLaning;


    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public List<Scene> getScenePLaning() {
        return scenePLaning;
    }

    public void setScenePLaning(List<Scene> scenePLaning) {
        this.scenePLaning = scenePLaning;
    }

    @Override
    public String toString() {
        return "PlanningPlateau{" +
                "plateau=" + plateau +
                ", scenePLaning=" + scenePLaning +
                '}';
    }

    public PlanningPlateau() {
        this.scenePLaning = new ArrayList<>();
    }
}
