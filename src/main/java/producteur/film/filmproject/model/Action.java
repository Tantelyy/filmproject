package producteur.film.filmproject.model;

import javax.persistence.*;

@Entity
public class Action {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "scenario")
    private String scenario;

    @Column(name = "mouvement")
    private String mouvement;

    @Column(name = "duree")
    private Double duree;

    @OneToOne
    @JoinColumn(name = "idPersonnage")
    private Personnage personnage;

    @OneToOne
    @JoinColumn(name = "idHumeur")
    private Humeur humeur;

    @Transient
    private Integer idPersonnage;

    @Transient
    private Integer idFilm;

    @Transient
    private Integer idHumeur;

    @Column(name = "idScene")
    private Integer idScene;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public Double getDuree() {
        return duree;
    }

    public void setDuree(Double duree) {
        this.duree = duree;
    }

    public Personnage getPersonnage() {
        return personnage;
    }

    public void setPersonnage(Personnage personnage) {
        this.personnage = personnage;
    }

    public Integer getIdPersonnage() {
        return idPersonnage;
    }

    public void setIdPersonnage(Integer idPersonnage) {
        this.idPersonnage = idPersonnage;
    }

    public Integer getIdScene() {
        return idScene;
    }

    public String getMouvement() {
        return mouvement;
    }

    public void setMouvement(String mouvement) {
        this.mouvement = mouvement;
    }

    public Humeur getHumeur() {
        return humeur;
    }

    public void setHumeur(Humeur humeur) {
        this.humeur = humeur;
    }

    public void setIdScene(Integer idScene) {
        this.idScene = idScene;
    }

    public Integer getIdHumeur() {
        return idHumeur;
    }

    public void setIdHumeur(Integer idHumeur) {
        this.idHumeur = idHumeur;
    }

    public Integer getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(Integer idFilm) {
        this.idFilm = idFilm;
    }

    @Override
    public String toString() {
        return "Action{" +
                "id=" + id +
                ", scenario='" + scenario + '\'' +
                ", mouvement='" + mouvement + '\'' +
                ", duree=" + duree +
                ", personnage=" + personnage +
                ", humeur=" + humeur +
                ", idPersonnage=" + idPersonnage +
                ", idScene=" + idScene +
                ", idHumeur="+ idHumeur +
                '}';
    }
}
