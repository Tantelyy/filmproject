package producteur.film.filmproject.model;

import javax.persistence.*;
import java.sql.Time;
import java.util.List;
import java.util.Date;

@Entity
public class V_scene {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "titre")
    private String titre;

    @Column(name = "image")
    private String image;

    @Transient
    private Integer idPlateau;

    @Column(name = "heureMin")
    private Time heureMin;

    @Column(name = "heureMax")
    private Time heureMax;

    @Column(name = "idFilm")
    private Integer idFilm;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "idScene")
    private List<Action> actions;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, targetEntity = Plateau.class)
    @JoinColumn(name = "idPlateau")
    private Plateau plateau;

    private Double duree;
    @Transient
    private Date debutPlanning;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getIdPlateau() {
        return idPlateau;
    }

    public void setIdPlateau(Integer idPlateau) {
        this.idPlateau = idPlateau;
    }

    public Time getHeureMin() {
        return heureMin;
    }

    public void setHeureMin(Time heureMin) {
        this.heureMin = heureMin;
    }

    public Time getHeureMax() {
        return heureMax;
    }

    public void setHeureMax(Time heureMax) {
        this.heureMax = heureMax;
    }

    public Integer getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(Integer idFilm) {
        this.idFilm = idFilm;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public Double getDuree() {
        return duree;
    }

    public void setDuree(Double duree) {
        this.duree = duree;
    }

    public Date getDebutPlanning() {
        return debutPlanning;
    }

    public void setDebutPlanning(Date debutPlanning) {
        this.debutPlanning = debutPlanning;
    }

    @Override
    public String toString() {
        return "V_scene{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", image='" + image + '\'' +
                ", idPlateau=" + idPlateau +
                ", heureMin=" + heureMin +
                ", heureMax=" + heureMax +
                ", idFilm=" + idFilm +
                ", actions=" + actions +
                ", plateau=" + plateau +
                ", duree=" + duree +
                ", debutPlanning=" + debutPlanning +
                '}';
    }
}
