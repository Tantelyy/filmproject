<%@ page import="java.util.List" %>
<%@ page import="producteur.film.filmproject.model.Plateau" %>
<%@ page import="producteur.film.filmproject.model.Scene" %>
<%@ page import="producteur.film.filmproject.model.Personnage" %>
<%@ page import="producteur.film.filmproject.model.Humeur" %><%--
Created by IntelliJ IDEA.
User: tantely
Date: 2023-03-11
Time: 19:14
To change this template use File | Settings | File Templates.
--%>
<%
  int idFilm = (Integer) request.getAttribute("idFilm");
  List<Plateau> plateaux = (List<Plateau>) request.getAttribute("plateaux");
  List<Scene> scenes = (List<Scene>) request.getAttribute("scenes");
  System.out.println("taille = " + scenes.size());
  List<Personnage> personnages = (List<Personnage>) request.getAttribute("personnages");
  List<Humeur> humeurs = (List<Humeur>) request.getAttribute("humeurs");
%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <!-- ===============================================-->
  <!--    Document Title-->
  <!-- ===============================================-->
  <title>Scene | Film</title>

  <!-- ===============================================-->
  <!--    Favicons-->
  <!-- ===============================================-->
  <link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/resources/theme/assets/img/favicons/apple-touch-icon.png" />
  <link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/resources/theme/assets/img/favicons/favicon-32x32.png" />
  <link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/resources/theme/assets/img/favicons/favicon-16x16.png" />
  <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/theme/assets/img/favicons/favicon.ico" />
  <link rel="manifest" href="${pageContext.request.contextPath}/resources/theme/assets/img/favicons/manifest.json" />
  <meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/theme/assets/img/favicons/mstile-150x150.png" />
  <meta name="theme-color" content="#ffffff" />

  <!-- ===============================================-->
  <!--    Stylesheets-->
  <!-- ===============================================-->
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;500;600;700;800&amp;display=swap"
        rel="stylesheet" />
  <link href="${pageContext.request.contextPath}/resources/theme/vendors/prism/prism.css" rel="stylesheet" />
  <link href="${pageContext.request.contextPath}/resources/theme/vendors/swiper/swiper-bundle.min.css" rel="stylesheet" />
  <link href="${pageContext.request.contextPath}/resources/theme/assets/css/theme.css" rel="stylesheet" />
  <link href="${pageContext.request.contextPath}/resources/theme/assets/css/user.css" rel="stylesheet" />
  <style>
    input[type="text"],
    select,
    input[type="number"] {
      width: 100%;
      padding: 12px 20px;
      margin: 8px 0;
      display: inline-block;
      border: 1px solid #ccc;
      box-sizing: border-box;
    }

    /* Set a style for all buttons */
    button {
      background-color: #4caf50;
      color: white;
      padding: 14px 20px;
      margin: 8px 0;
      border: none;
      cursor: pointer;
      width: 100%;
    }

    button:hover {
      opacity: 0.8;
    }

    /* Extra styles for the cancel button */
    .cancelbtn {
      width: auto;
      padding: 10px 18px;
      background-color: #f46c36;
    }

    .milieu {
      text-align: left;
      margin-top: 20px;
    }

    /* Center the image and position the close button */
    .imgcontainer {
      text-align: center;
      margin: 24px 0 12px 0;
      position: relative;
    }

    img.avatar {
      width: 40%;
      border-radius: 50%;
    }

    .container {
      padding: 16px;
    }

    span.psw {
      float: right;
      padding-top: 16px;
    }

    /* The Modal (background) */
    .modal {
      display: none;
      position: fixed;
      /*Stay in place*/
      z-index: 1;
      /* Sit on top */
      left: 0;
      top: 0;
      overflow: auto;
      /* Enable scroll if needed */
      background-color: rgb(0, 0, 0);
      /* Fallback color */
      background-color: rgba(0, 0, 0, 0.4);
      /* Black w/ opacity */
      padding-top: 60px;
    }

    /* Modal Content/Box */
    .modal-content {
      background-color: #fefefe;
      margin: 5% auto 15% auto;
      /* 5% from the top, 15% from the bottom and centered */
      border: 1px solid #888;
      width: 80%;
      /* Could be more or less, depending on screen size */
    }

    /* The Close Button (x) */
    .close {
      position: absolute;
      right: 25px;
      top: 0;
      color: #000;
      font-size: 35px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: red;
      cursor: pointer;
    }

    /* Add Zoom Animation */
    .animate {
      -webkit-animation: animatezoom 0.6s;
      animation: animatezoom 0.6s;
    }

    @-webkit-keyframes animatezoom {
      from {
        -webkit-transform: scale(0);
      }

      to {
        -webkit-transform: scale(1);
      }
    }

    @keyframes animatezoom {
      from {
        transform: scale(0);
      }

      to {
        transform: scale(1);
      }
    }

    /* Change styles for span and cancel button on extra small screens */
    @media screen and (max-width: 300px) {
      span.psw {
        display: block;
        float: none;
      }

      .cancelbtn {
        width: 100%;
      }
    }
  </style>
</head>

<body>
<div id="contenu">
  <!-- ===============================================-->
  <!--    Main Content-->
  <!-- ===============================================-->
  <main class="main" id="top">
    <div class="cursor-outer d-none d-md-block"></div>
    <nav class="navbar navbar-light py-3 px-0 overflow-hidden">
      <div class="container px-md-5">
        <div class="row w-100 g-0 justify-content-between">
          <div class="col-8">
            <div class="d-inline-block">
              <a class="navbar-brand pt-0 fs-3 text-black d-flex align-items-center" href="index.html"><img
                      class="img-fluid" src="${pageContext.request.contextPath}/resources/theme/assets/img/icons/logo-icon.png" alt="" /><span
                      class="fw-bolder ms-2">Foto</span><span class="fw-thin">gency</span></a>
            </div>
            <div class="mt-4 d-none d-lg-block">
              <h1 class="text-uppercase fs-lg-7 fs-5 fw-bolder text-300 lh-1 position-relative z-index-0">Scene</h1>
              <h1
                      class="d-none d-md-block fw-bolder text-outlined fs-7 text-white lh-1 mt-n4 position-relative z-index--1">
                Recent update</h1>
            </div>
          </div>
          <div class="col-4 d-lg-none text-end pe-0">
            <button class="btn p-0 shadow-none text-black fs-2 d-inline-block" data-bs-toggle="offcanvas"
                    data-bs-target="#navbarOffcanvas" aria-controls="navbarOffcanvas" aria-expanded="false"
                    aria-label="Toggle offcanvas navigation">
              <span class="menu-bar"></span>
            </button>
          </div>
          <div class="offcanvas offcanvas-end px-0" id="navbarOffcanvas" aria-labelledby="navbarOffcanvasTitle"
               aria-hidden="true">
            <div class="offcanvas-header justify-content-end">
              <h5 class="visually-hidden offcanvas-title" id="navbarOffcanvasTitle">
                Menu
              </h5>
              <button class="btn p-0 shadow-none text-black fs-2 d-inline text-end" type="button"
                      data-bs-dismiss="offcanvas" aria-label="Close">
                <span class="menu-close-bar"></span>
              </button>
            </div>
          </div>
          <div class="col-4">
            <div class="d-none d-lg-flex justify-content-end position-relative z-index-1">
              <div class="position-absolute absolute-centered z-index--1">
                <h1 class="ms-2 fw-bolder text-outlined text-uppercase text-white pe-none display-1">
                  Scene
                </h1>
              </div>
              <div class="d-flex gap-3 align-items-start">
                <ul class="navbar-nav navbar-fotogency ms-auto text-end">
                  <li class="nav-item px-2 position-relative">
                    <a class="nav-link pt-0 active" aria-current="page" href="<%= request.getContextPath() %>/">Home</a>
                  </li>
                  <li class="nav-item px-2 position-relative">
                    <a class="nav-link pt-0" aria-current="page" href="<%= request.getContextPath() %>/indisponibilite">Indisponibilité</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row g-0 mt-4 d-lg-none">
          <h1 class="text-uppercase ps-0 fs-lg-7 fs-5 fw-bolder text-300 lh-1 position-relative z-index-0">
            Scene
          </h1>
          <h1
                  class="fw-bolder text-outlined ps-0 fs-lg-7 fs-sm-6 fs-5 text-white lh-1 mt-sm-n4 mt-n3 position-relative z-index--1">
            Recent update
          </h1>
        </div>
      </div>
    </nav>

    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="py-4" id="contact">
      <div class="container px-md-5">
        <div class="row align-items-center">
          <div class="my-4 text-center">
            <h1 class="text-uppercase fs-lg-8 fs-6 fw-bolder text-300 lh-1 position-relative z-index-0">
              Scene
            </h1>
            <h1 class="fw-bolder text-outlined fs-7 text-white lh-1 mt-n4 position-relative z-index--1">
              Add scene
            </h1>
            <form action="<%= request.getContextPath() %>/addScene" method="post" enctype="multipart/form-data" class="my-5 px-3">
              <input type="hidden" name="idFilm" value="<%= idFilm %>">
              <div class="row g-3 g-md-5">
                <div class="col-md-6">
                  <div class="card border-0">
                    <div class="form-floating mb-3">
                      <input name="titre" class="form-control form-fotogency-header-control" id="floatingEmail" type="text" />
                      <label for="floatingEmail">Titre</label>
                    </div>

                    <div class="form-floating mb-3">
                      <select name="idPlateau" class="form-control form-fotogency-header-control">
                        <% for (Plateau plateau: plateaux) { %>
                        <option value="<%= plateau.getId() %>"><%= plateau.getNom() %></option>
                        <% } %>
                      </select>
                      <label class="ps-2" for="floatingTextarea">Plateau</label>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="milieu col-md-4">
                        <label>Heure ideal debut:</label>
                      </div>
                      <div class="col-md-8">
                        <input class="form-control form-fotogency-header-control" type="time" name="min" id="min"  />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card border-0">
                    <!-- eto sary -->
                    <div class="col-md-12">
                      <div class="row">
                        <div class="milieu col-md-4 mb-3">
                          <label for="floatingEmail">Image</label>
                        </div>
                        <div class="col-md-8 mb-3">
                          <input class="form-control form-fotogency-header-control" type="file" name="file" id="selectImage" />
                          <input type="hidden" name="visual" id="imageCode" />
                          <!-- <img src="" id="imageFilm"/> -->
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-4 milieu" >
                        <label>Heure ideal fin:</label>
                      </div>
                      <div class="col-md-8">
                        <input class="form-control form-fotogency-header-control" type="time" name="max" />
                      </div>
                    </div>
                  </div>
                </div>
                <div>
                  <button class="btn btn-link text-warning p-0" type="submit">
                    <span class="fw-bolder text-uppercase">Valider</span>
                    <img class="ms-3" src="${pageContext.request.contextPath}/resources/theme/assets/img/icons/long-arrow.png" alt="" />
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <!-- end of .container-->
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->

    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="py-2">
      <div class="container px-md-5">
        <div class="row g-4">
          <% for (Scene scene: scenes) { %>
          <div class="col-lg-4 col-md-6 d-flex">
            <div class="card border-0 mb-3">
              <img class="card-img-top" src="<%= request.getContextPath() %>/images/<%= scene.getImage() %>" alt="" />

              <div class="mt-4">
                <h5 class="mb-3">
                  <%= scene.getTitre() %>
                </h5>
                <p class="text-200 m-0 mb-3">
                  Plateau:
                  <span class="text-300 fw-bold"><%= scene.getPlateau().getNom() %></span>
                </p>
                <% if(scene.getStatut().getId() < 3) { %>

                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">
                      <button class="btn btn-outline-warning btn-sm"
                              onclick="addAction(<%= scene.getId() %>,<%= idFilm %> )">
                        <span class="fw-medium">Ajouter action</span>
                      </button>
                    </div>
                    <div class="col-md-6">
                      <a href="<%= request.getContextPath() %>/terminate?idScene=<%= scene.getId() %>&&idFilm=<%= idFilm %>"><button class="btn btn-success btn-sm">
                        <span class="fw-medium">Terminer</span>
                        </button>
                      </a>
                    </div>
                  </div>
                </div>
                <% } %>
              </div>
            </div>
          </div>
          <% } %>
        </div>
      </div>
      <!-- end of .container-->
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->
  </main>
  <!-- ===============================================-->
  <!--    End of Main Content-->
  <!-- ===============================================-->
  <!--  Modal  -->
  <!-- ===============================================-->
  <div id="id01" class="modal col-md-6">
    <div class="row g-3 g-md-5">
      <form class="modal-content animate my-5 px-3" action="<%= request.getContextPath() %>/addAction" method="post">
        <div class="imgcontainer">
            <span onclick="document.getElementById('id01').style.display='none'" class="close"
                  title="Close Modal">&times;</span>
          <!-- <img src="img_avatar2.png" alt="Avatar" class="avatar" /> -->
          <h3 class="avatar">Ajout Action</h3>
        </div>
        <input type="hidden" id="idScene" name="idScene" value=""/>
        <input type="hidden" id="idFilm" name="idFilm" value=""/>
        <div class="container">
          <div class="row g-3 g-md-5">
            <div class="col-md-6">
              <label for="uname"><b>Personnage</b></label>
              <select class="form-fotogency-header-control" name="idPersonnage">
                <option value=""></option>
                <% for (Personnage personnage: personnages) { %>
                  <option value="<%= personnage.getId() %>"><%= personnage.getNom() %></option>
                <% } %>
              </select>

              <label for="uname"><b>Mouvement</b></label>
              <input class="form-control form-fotogency-header-control" type="text" id="uname" name="mouvement" />

              <label for="uname"><b>Humeur</b></label>
              <select name="idHumeur">
                <option value=""></option>
                <% for (Humeur humeur: humeurs) { %>
                  <option value="<%= humeur.getId() %>"><%= humeur.getValeur() %></option>
                <% } %>
              </select>

              <label for="psw"><b>Duree</b></label>
              <input class="form-control form-fotogency-header-control" type="number"  step="any" name="duree" id="psw" />
            </div>
            <div class="col-md-6">
              <label for="scenario"><b>Scenario</b></label>
              <!-- <div class="form-floating mb-3"> -->
              <input class="form-control opacity-0 pb-0 pe-none d-none d-md-block" id="scenario" />
              <textarea class="form-control form-fotogency-header-control border ps-2 form-fotogency-header-control"
                        name="scenario" id="floatingTextarea" style="height: 210px"></textarea>
              <!-- </div> -->
            </div>
          </div>
          <button type="submit" style="background-color: #888">
            Ajouter action
          </button>
        </div>
      </form>
    </div>
  </div>
  <!--  End Modal  -->
  <!--================================================-->

  <footer class="bg-footer-black" id="footer">
    <div class="container py-7 px-md-5">
      <div class="row g-0">
        <div class="col-md-6 mb-5 mb-md-0">
          <div class="mb-4 d-inline-block">
            <a class="navbar-brand pt-0 fs-3 text-white d-flex align-items-center" href="index.html"><img
                    class="img-fluid" src="${pageContext.request.contextPath}/resources/theme/assets/img/icons/dark-logo-icon.png" alt="" width="25" height="25" /><span
                    class="fw-bolder ms-2">Foto</span><span class="fw-thin">gency</span></a>
          </div>
          <div class="col-12 col-md-10 text-300 mb-3">
            <i class="fas fa-map-marker-alt"></i><a class="nav-link text-300 d-inline"
                                                    href="https://www.google.com/maps/place/2109+WS+09+Oxford+Rd+%23127+ParkVilla+Hills,+">2109 WS 09 Oxford
            Rd #127 ParkVilla Hills, MI 48334</a>
          </div>
          <div class="col-12 col-md-10 text-300 mb-3">
            <i class="fas fa-phone"></i><a class="nav-link text-300 d-inline" href="tel:+8801976476893">+88 019
            76456893</a>
          </div>
          <div class="col-12 col-md-10 text-300 mb-3">
            <i class="fas fa-envelope"></i><a class="nav-link text-300 d-inline"
                                              href="mailto:scrscrumbledegg@gmail.com">scrumbledegg@gmail.com</a>
          </div>
        </div>
        <div class="col-md-6">
          <h5 class="text-white mb-2">SUBSCRIBE TO OUR NEWSLETTER</h5>
          <form>
            <div class="form-floating mb-5 position-relative text-200">
              <input class="form-control form-fotogency-header-control text-200" id="email" type="email"
                     placeholder="name@example.com" required="required" />
              <label class="text-300 mb-1" for="email">Email</label>
              <div class="position-absolute end-0 top-0">
                <button class="btn shadow-none px-0" type="submit">
                  <img class="ms-3" src="${pageContext.request.contextPath}/resources/theme/assets/img/icons/long-arrow.png" alt="" />
                </button>
              </div>
            </div>
          </form>
          <div class="d-flex gap-2 flex-wrap justify-content-between align-items-center">
            <a class="social-icons" href="https://www.facebook.com/" target="_blank">Facebook</a><a
                  class="social-icons" href="https://www.flickr.com/photos/" target="_blank">Flickr</a><a
                  class="social-icons" href="https://twitter.com/" target="_blank">Twitter</a><a class="social-icons"
                                                                                                 href="https://www.instagram.com/" target="_blank">Instagram</a><a class="social-icons"
                                                                                                                                                                   href="https://www.youtube.com/" target="_blank">Youtube</a>
          </div>
        </div>
      </div>
    </div>
  </footer>

  <!-- ============================================-->
  <!-- <section> begin ============================-->
  <section class="bg-black py-0">
    <div class="container px-md-5">
      <div class="row g-0 justify-content-md-between justify-content-evenly py-4">
        <div class="col-12 col-sm-8 col-md-6 col-lg-auto text-center text-md-start">
          <p class="fs--1 my-2 fw-light text-100">
            All rights Reserved &copy; Your Company, 2022
          </p>
        </div>
        <div class="col-12 col-sm-8 col-md-6">
          <p class="fs--1 fw-light my-2 text-center text-md-end text-100">
            Made with&nbsp;
            <svg class="bi bi-suit-heart-fill" xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                 fill="#F95C19" viewBox="0 0 16 16">
              <path
                      d="M4 1c2.21 0 4 1.755 4 3.92C8 2.755 9.79 1 12 1s4 1.755 4 3.92c0 3.263-3.234 4.414-7.608 9.608a.513.513 0 0 1-.784 0C3.234 9.334 0 8.183 0 4.92 0 2.755 1.79 1 4 1z">
              </path>
            </svg>
            >&nbsp;by&nbsp;<a class="fw-bold text-warning" href="https://themewagon.com/"
                              target="_blank">ThemeWagon</a>
          </p>
        </div>
      </div>
    </div>
    <!-- end of .container-->
  </section>
  <!-- <section> close ============================-->
  <!-- ============================================-->
</div>

<!-- ===============================================-->
<!--    JavaScripts-->
<!-- ===============================================-->
<script src="${pageContext.request.contextPath}/resources/theme/vendors/popper/popper.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/bootstrap/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/anchorjs/anchor.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/is/is.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/fontawesome/all.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/lodash/lodash.min.js"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/prism/prism.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/vendors/swiper/swiper-bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/theme/assets/js/theme.js"></script>
<script>
  const input = document.getElementById("selectImage");
  const imageCode = document.getElementById("imageCode");

  const convertBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onload = () => {
        resolve(fileReader.result);
      };

      fileReader.onerror = (error) => {
        reject(error);
      };
    });
  };
  const uploadImage = async (event) => {
    const file = event.target.files[0];
    const base64 = await convertBase64(file);
    imageCode.value = base64;
    //console.log(imageCode.value);
    return base64;
  };

  input.addEventListener("change", async (e) => {
    //imageFilm
    let base64 = await uploadImage(e);
    document.getElementById("imageFilm").src = base64;
    document.getElementById("imageFilm").style.display = "block";
  });

  function addAction(id, idF) {
    console.log(id);
    let idScene = document.getElementById('idScene');
    idScene.value = id;
    let idFilm = document.getElementById('idFilm');
    idFilm.value = idF;
    document.getElementById('id01').style.display='block';
  }
</script>
</body>

</html>

