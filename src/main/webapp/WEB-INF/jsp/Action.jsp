<%@ page import="java.util.List" %>
<%@ page import="producteur.film.filmproject.model.Personnage" %>
<%@ page import="producteur.film.filmproject.model.Humeur" %><%--
  Created by IntelliJ IDEA.
  User: tantely
  Date: 2023-03-11
  Time: 20:32
  To change this template use File | Settings | File Templates.
--%>
<%
  int idScene = (Integer) request.getAttribute("idScene");
  List<Personnage> personnages = (List<Personnage>) request.getAttribute("personnages");
  List<Humeur> humeurs = (List<Humeur>) request.getAttribute("humeurs");
%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Action</title>
</head>
<body>
  <form action="<%= request.getContextPath() %>/addAction" method="post">
    <input type="hidden" value="<%= idScene %>" name="idScene">
    <p>Personnage:<select name="idPersonnage">
      <option value=""></option>
      <% for (Personnage personnage: personnages) { %>
        <option value="<%= personnage.getId() %>"><%= personnage.getNom() %></option>
      <% } %>
    </select></p>
    <p>Humeur: <select name="idHumeur">
      <option value=""></option>
      <% for (Humeur humeur: humeurs) { %>
        <option value="<%= humeur.getId() %>"><%= humeur.getValeur() %></option>
      <% } %>
    </select></p>
    <p>Mouvement: <input type="text" name="mouvement"></p>
    <p>Scenario: <textarea name="scenario"></textarea></p>
    <p>Duree: <input type="duree" step="any" name="duree"></p>
    <input type="submit" value="Ajouter action" >
  </form>

  <div style="margin-left: 500px;">
    <nav aria-label="Page navigation example">
      <ul class="pagination">
        <li class="page-item">
          <a class="page-link" href="#" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
            <span class="sr-only">Previous</span>
          </a>
        </li>
        <li class="page-item text-warning" ><a class="page-link" href="#">1</a></li>
        <li class="page-item text-warning" ><a class="page-link" href="#">2</a></li>
        <li class="page-item text-warning" ><a class="page-link" href="#">3</a></li>
        <li class="page-item text-warning" >
          <a class="page-link" href="#" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
            <span class="sr-only">Next</span>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</body>
</html>
