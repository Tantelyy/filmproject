CREATE DATABASE film;
CREATE USER film WITH PASSWORD 'film';
ALTER DATABASE film OWNER TO film;

CREATE TABLE Personnage (
    id SERIAL PRIMARY KEY NOT NULL,
    nom VARCHAR(70) NOT NULL
);

CREATE TABLE Film (
    id SERIAL PRIMARY KEY NOT NULL,
    titre VARCHAR(70) NOT NULL,
    description TEXT DEFAULT NULL,
    photo TEXT NOT NULL
);

CREATE TABLE Plateau (
    id SERIAL PRIMARY KEY NOT NULL,
    nom VARCHAR(100) NOT NULL,
    longitude REAL NOT NULL,
    latitude REAL NOT NULL
);

CREATE TABLE Scene (
  id SERIAL PRIMARY KEY NOT NULL,
  titre VARCHAR(70) NOT NULL,
  idPlateau INTEGER NOT NULL,
  image TEXT NOT NULL,
  idFilm INTEGER NOT NULL,
  heureMax TIME,
  heureMin TIME CHECK (heureMin < heureMax),
  idStatut INTEGER DEFAULT 1,
  datePlanning TIMESTAMP DEFAULT NULL
);

CREATE TABLE Statut (
    id SERIAL PRIMARY KEY,
    statut VARCHAR(70) NOT NULL
);

CREATE TABLE Humeur (
    id SERIAL PRIMARY KEY,
    valeur VARCHAR(100) NOT NULL
);

CREATE TABLE Action (
    id SERIAL PRIMARY KEY NOT NULL,
    scenario TEXT NOT NULL,
    mouvement TEXT DEFAULT NULL,
    idHumeur INTEGER DEFAULT NULL,
    idPersonnage INTEGER DEFAULT NULL,
    idScene INTEGER NOT NULL,
    duree REAL NOT NULL
);

CREATE TABLE Indisponibilite (
    idPlateau INTEGER NOT NULL,
    jour Date NOT NULL,
    observation VARCHAR(255)
);

CREATE TABLE FilmPlanning (
    idFilm INTEGER NOT NULL,
    dateDebutTournage Date NOT NULL,
    heureTotalJournalier TIME NOT NULL,
    heureDebutJournalier TIME NOT NULL,
    weekEndImplique BOOLEAN NOT NULL
);

CREATE TABLE TableTemporaire (
    idScene INTEGER NOT NULL UNIQUE,
    planning TIMESTAMP NOT NULL
);

ALTER TABLE FilmPlanning ADD FOREIGN KEY (idFilm) REFERENCES Film(id);

ALTER TABLE Scene ADD FOREIGN KEY (idPlateau) REFERENCES  Plateau(id);
ALTER TABLE Scene ADD FOREIGN KEY (idFilm) REFERENCES  Film(id);
ALTER TABLE Scene ADD FOREIGN KEY (idStatut) REFERENCES Statut(id);

ALTER TABLE Action ADD FOREIGN KEY (idHumeur) REFERENCES  Humeur(id);
ALTER TABLE Action ADD FOREIGN KEY (idPersonnage) REFERENCES Personnage(id);
ALTER TABLE Action ADD FOREIGN KEY (idScene) REFERENCES Scene(id);

ALTER TABLE Indisponibilite ADD FOREIGN KEY (idPlateau) REFERENCES Plateau(id);

ALTER TABLE TableTemporaire ADD FOREIGN KEY (idScene) REFERENCES Scene(id);

CREATE OR REPLACE VIEW v_scene AS(
    SELECT distinct
(s.id),s.titre,s.idplateau,s.heureMin,s.heuremax,s.image,sum(a.duree)as duree,s.idFilm, s.dateplanning, s.idstatut
    FROM ACTION A JOIN SCENE S ON A.idSCENE = S.id 
    GROUP BY s.id,s.titre,s.idplateau,s.heureMin,s.heuremax,s.idFilm 
    ORDER BY s.id ASC
);

CREATE OR REPLACE VIEW v_plateau AS(
    SELECT v.*,p.longitude,p.latitude FROM v_scene v JOIN plateau p ON v.idplateau = p.id
);

CREATE OR REPLACE VIEW v_suggestion AS(
    SELECT s.id, s.titre, s.idfilm, s.image, s.idplateau, s.heuremin, s.heuremax, s.idstatut, t.planning as dateplanning
    FROM Scene s JOIN TableTemporaire t
    ON s.id = t.idscene
);

CREATE OR REPLACE VIEW v_suggestion2 AS(
    SELECT * FROM Scene WHERE idstatut=4 UNION SELECT * FROM v_suggestion
);

