INSERT INTO Personnage (nom) VALUES
('Franck'),
('Lova'),
('Lalaina'),
('Adrien'),
('Marinah'),
('Josephine');

INSERT INTO Humeur (valeur) VALUES
('Faly'),
('Tezitra'),
('Malahelo'),
('Gaga');

INSERT INTO Statut (statut) VALUES
('Cree'),
('En cours d''ecriture'),
('Ecriture termine'),
('Planifie');

INSERT INTO Plateau (nom, longitude, latitude) VALUES
('Plateau 1', 10.2154543, 10.5646543),
('Plateau 2', 09.5, 11.08);


INSERT INTO Film (titre, description, photo) VALUES
('Malok''ila', 'Natao ho an''ny mpianakavy', 'malokila sary'),
('Vola', 'Mandreraka', 'vola sary');

-- INSERT INTO Scene (titre, idPlateau, image, idfilm, heureMin, heureMax) VALUES
-- ('Mifamaly', 1, 'sary mifamaly', 2, '08:00:00', '10:00:00'),
-- ('Mivavaka', 4, 'olona marobe mivavaka', 2, '10:00:00', '12:00:00');



INSERT INTO SCENE(id,titre,idPlateau,image,idFilm,heuremin,heuremax) VALUES
(default,'SCENE1',1,'scene1',1,'08:00:00','11:00:00'),
(default,'SCENE2',1,'scene2',1,'12:00:00','15:00:00'),
(default,'SCENE3',1,'scene3',1,'08:00:00','10:00:00'),
(default,'SCENE4',1,'scene4',1,'11:00:00','14:00:00'),
(default,'SCENE5',1,'scene5',1,'14:30:00','16:00:00'),
(default,'SCENE6',1,'scene6',1,'08:00:00','09:40:00'),
(default,'SCENE7',1,'scene7',1,'13:00:00','15:00:00'),
(default,'SCENE8',2,'scene8',1,'09:00:00','12:00:00'),
(default,'SCENE9',2,'scene9',1,'08:20:00','09:00:00'),
(default,'SCENE10',2,'scene10',1,'09:30:00','10:30:00');

INSERT INTO Action (scenario, mouvement, idhumeur, idpersonnage, idscene, duree) VALUES
('Scenario 1', 'Mouvement 1', null, null, 1, 160),
('Scenario 2', 'Mouvement 2', 1, 2, 2, 60),
('Scenario 3', 'Mouvement 3', 3, 1, 3, 110),
('Scenario 4', 'Mouvement 4', null, null, 4, 40),
('Scenario 5', 'Mouvement 5', 4, 3, 5, 40),
('Scenario 6', 'Mouvement 6', 2, 2, 6, 80),
('Scenario 7', 'Mouvement 7', null, null, 7, 40),
('Scenario 8', 'Mouvement 8', 4, 1, 8, 40),
('Scenario 9', 'Mouvement 9', 2, 3, 9, 40),
('Scenario 10', 'Mouvement 10', null, null, 10, 40);


INSERT INTO indisponibilite (idPlateau, jour) VALUES
(1, '2023-03-25');

--INSERT INTO TableTemporaire (idScene, planning) VALUES
--(1, '2023-03-15 12:30:00');